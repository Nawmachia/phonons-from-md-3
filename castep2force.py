#!/usr/bin/env python3
################################################################
#
# Convert an input .md file (or set of .md files in a .gz)
# into suitable format for input to potfit
#
################################################################
#  Based on original version by Corwin Kuiper 2020
#################################################################

import argparse
import gzip
import sys
import castep_md_reader as md

from collections import OrderedDict

from typing import Dict, List, IO, Union

def parse_cli_flags(argv: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Converts a castep molecular dynamics output file (or set of files) to a potfit force configuration file",
        epilog="Example usage: ./castep2force -p Ar=0.01 -o configuration mdinput.md",
    )

    parser.add_argument(
        "files", type=str, nargs="+", help="castep molecular dynamics file",
    )
    parser.add_argument(
        "-p",
        "--potential",
        type=str,
        required=True,
        help="chemical potentials for the elements used in electronvolts in the format Element1=Potential1,Element2=Potential2. For example Ar=0.01,Si=0.01,Ne=0.01",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        required=False,
        help="file to output force configuration to, default is to standard output",
        default="-",
    )

    parser.add_argument(
        "-f",
        "--final",
        action="store_true",
        help="Only use the final configuration in each file",
    )

    return parser.parse_args(argv)

def fatal(message: str) -> None:
    if message.endswith("\n"):
        sys.stderr.write("FATAL: {}".format(message))
    else:
        sys.stderr.write("FATAL: {}\n".format(message))
    sys.exit(1)


def warning(message: str) -> None:
    if message.endswith("\n"):
        sys.stderr.write("WARN: {}".format(message))
    else:
        sys.stderr.write("WARN: {}\n".format(message))

def conf_to_potfit(conf: md.Configuration, potentials) -> str:

    #convert from atomic to physics units for potfit
    md.atomic_to_units(conf)

    #calculate cohesive energy from the input potential_energy & the chemical potentials
    coh_energy=conf.pot_energy   #in eV after atomic_to_units call
    if coh_energy is not None:
        number_of_atoms = 0
        for element,body in conf.body.items():
            if element not in potentials:
                fatal(
                    "Element '{}' in MD file, but no chemical potential given".format(element)
                )
            coh_energy -= potentials[element] * len(body)
            number_of_atoms += len(body)
        coh_energy /= number_of_atoms

    s = []
    # Example output - write configurations in suitable format for potfit
    s.append("#N {} 1".format(number_of_atoms))
    s.append("#C {}".format(" ".join(conf.types_of_element.keys())))
    s.append("#X {:23.16E} {:23.16E} {:23.16E}".format(
        conf.boxes[0].x,conf.boxes[0].y,conf.boxes[0].z))
    s.append("#Y {:23.16E} {:23.16E} {:23.16E}".format(
        conf.boxes[1].x,conf.boxes[1].y,conf.boxes[1].z))
    s.append("#Z {:23.16E} {:23.16E} {:23.16E}".format(
        conf.boxes[2].x,conf.boxes[2].y,conf.boxes[2].z))
    #potfit wants the cohesive energy per atom
    s.append("#E {:23.16E}".format(coh_energy if coh_energy else 0))

    #NB potfit does not care about temperature so don't output that here

    # Only output stresses if they are in the input file
    if len(conf.stress) == 3:
        # #S stress_xx stress_yy stress_zz stress_xy stress_yz stress_xz
        s.append(
            "#S {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E}".format(
                conf.stress[0][0],
                conf.stress[1][1],
                conf.stress[2][2],
                conf.stress[0][1],
                conf.stress[1][2],
                conf.stress[0][2],
            )
        )
    s.append("#F")
    # End of header
    # Start of body - potfit wants R & F but not V for every atom
    for _, value in conf.body.items():
        for atom in value:
            s.append(
                "{} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E}".format(
                    atom.type, atom.r_x,atom.r_y,atom.r_z,atom.f_x,atom.f_y,atom.f_z))
    
    # End with a newline
    s.append("")
    return "\n".join(s)

def parse_potentials(potential_str: str) -> Dict[str, float]:
    potentials = {}
    for pair in potential_str.split(","):
        values = pair.split("=")
        potentials[values[0].strip()] = float(values[1])
    return potentials


def main(argv: List[str]) -> None:
    import castep_md_reader as md
    
    arguments = parse_cli_flags(argv)
    filenames = arguments.files
    potentials = parse_potentials(arguments.potential)

    configurations = []  # type: List[Configuration]
    for filename in filenames:
        configuration = md.read_md_file(filename)
        if arguments.final:
            # add only the last configuration
            configurations.append(configuration[-1])
        else:
            # add all configurations
            configurations.extend(configuration)

    output_filename = arguments.output
    if output_filename == "-" or output_filename is None:
        for conf in configurations:
            sys.stdout.write(conf_to_potfit(conf,potentials))
    else:
        with open(output_filename, "w") as file:
            for conf in configurations:
                file.write(conf_to_potfit(conf,potentials))


if __name__ == "__main__":
    main(sys.argv[1:])
